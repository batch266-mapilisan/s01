package com.zuitt.activity;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        Scanner scanner = new Scanner(System.in);
        System.out.println("First Name:");
        firstName = scanner.nextLine();
        System.out.println("Last Name:");
        lastName = scanner.nextLine();
        System.out.println("First Subject Grade:");
        firstSubject = Double.parseDouble(scanner.nextLine());
        System.out.println("Second Subject Grade:");
        secondSubject = Double.parseDouble(scanner.nextLine());
        System.out.println("Third Subject Grade:");
        thirdSubject = Double.parseDouble(scanner.nextLine());
        System.out.println("Good day, " + firstName + " " + lastName);
        int average = (int)(firstSubject + secondSubject + thirdSubject) / 3;
        System.out.println("Your grade average is: " + average);

    }
}
