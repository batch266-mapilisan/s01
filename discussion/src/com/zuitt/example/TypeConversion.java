package com.zuitt.example;

import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args) {
        /*
        *   '2' + 2 = 22 for javascript
        *   System.out.println("2" + 2);
        * */
//        System.out.println("How old are you?");
//        Scanner myScanner = new Scanner(System.in);
//        String age = myScanner.nextLine();
//        System.out.println("The age is " + age);
        Scanner userInput = new Scanner(System.in);
        System.out.println("How old are you?");
        double age = new Double(userInput.nextLine());

        System.out.println("This is a confirmation that you are " + age + " years old.");
    }
}
